# Contributor: Eric Molitor <eric@molitor.org>
# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Rasmus Thomsen <oss@cogitri.dev>
pkgname=lld
pkgver=14.0.4
pkgrel=1
_llvmver=${pkgver%%.*}
pkgdesc="The LLVM Linker"
url="https://llvm.org/"
# s390x: limited by llvm-libunwind
arch="all !s390x"
license="Apache-2.0"
makedepends="
	cmake
	libedit-dev
	llvm$_llvmver-dev
	llvm$_llvmver-static
	llvm$_llvmver-test-utils
	llvm-libunwind-dev
	ninja
	xz
	zlib-dev
	"
checkdepends="gtest gtest-dev bash llvm$_llvmver-test-utils"
subpackages="$pkgname-static $pkgname-dev $pkgname-doc"
source="https://github.com/llvm/llvm-project/releases/download/llvmorg-$pkgver/lld-$pkgver.src.tar.xz"
builddir="$srcdir/$pkgname-$pkgver.src"

# Tests OOM on 32-bit
case "$CARCH" in
	s390x|x86|armhf|armv7) options="!check" ;;
esac

build() {
	cmake -B build -G Ninja -Wno-dev \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_EXE_LINKER_FLAGS_MINSIZEREL_INIT="$LDFLAGS -Wl,-z,stack-size=2097152" \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_SKIP_INSTALL_RPATH=ON \
		-DLLVM_INCLUDE_TESTS=ON \
		-DLLVM_EXTERNAL_LIT=/usr/bin/lit \
		-DLLD_BUILT_STANDALONE=ON
	cmake --build build
}

check() {
	# ELF/eh-frame-hdr-augmentation.s is broken on armhf & armv7
	case "$CARCH" in
		armhf|armv7) rm test/ELF/eh-frame-hdr-augmentation.s;;
	esac

	ninja -C build check-lld
}

package() {
	DESTDIR="$pkgdir" cmake --install build
	install -Dm 644 $builddir/docs/ld.lld.1 -t $pkgdir/usr/share/man/man1/
}

sha512sums="
69d9857c9a8c845f84b3fc9f6e703d5d0bab19576c5dcbe2c798b19c717f657c6a5c41790973407d2ea2717f011fbcc160d150315e6230e88740ae95ef7ad4e0  lld-14.0.4.src.tar.xz
"
