# Contributor: Bartłomiej Piotrowski <bpiotrowski@alpinelinux.org>
# Maintainer: Rasmus Thomsen <oss@cogitri.dev>
pkgname=lldb
# Note: Update together with llvm.
pkgver=14.0.4
pkgrel=0
_llvmver=${pkgver%%.*}
pkgdesc="Next generation, high-performance debugger"
# riscv64 fails to build
arch="all !x86 !riscv64"
url="https://llvm.org/"
license="Apache-2.0"
makedepends="
	clang-dev>=$_llvmver
	clang-static>=$_llvmver
	cmake
	doxygen
	gtest-dev
	libedit-dev
	libffi-dev
	libxml2-dev
	linux-headers
	llvm-dev>=$_llvmver
	llvm-static>=$_llvmver
	ncurses-dev
	ninja
	python3-dev
	swig
	xz
	"
subpackages="$pkgname-dev py3-$pkgname:py3"
source="https://github.com/llvm/llvm-project/releases/download/llvmorg-$pkgver/lldb-$pkgver.src.tar.xz
	fix-embedded_interpreter.patch
	musl-compat.patch
	"
builddir="$srcdir/$pkgname-$pkgver.src"
options="!check"  # FIXME: enable tests

build() {
	CC=clang CXX=clang++ cmake -B build -G Ninja -Wno-dev \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DLLVM_LINK_LLVM_DYLIB=ON \
		-DLLDB_INCLUDE_TESTS="$(want_check && echo ON || echo OFF)" \
		-DLLDB_BUILT_STANDALONE=ON
	cmake --build build
}

check() {
	ninja -C build check-lldb
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

py3() {
	pkgdesc="Python3 module for LLDB"
	depends="$pkgname python3 py3-six"
	replaces="py-lldb"

	local sitedir=$(python3 -c "import site; print(site.getsitepackages()[0])")

	amove "$sitedir"

	# Remove bundled module.
	rm "$subpkgdir"/"$sitedir"/six.py

	python3 -m compileall -fq "$subpkgdir"/"$sitedir"
}

sha512sums="
bc16ab06548de194ded389a0af1c093dc46007f3921fb02d28f59e8d9d950038260c9c492b2a790573334c3819806a384c0cad96d370801b92ce26d49f3d3da8  lldb-14.0.4.src.tar.xz
3c611fa5d45b6cb3f2925a31deeb8a34c295277aedcd55c22851d373897acd376fa92f4ef953c96a25c8dae4c93b6a88de0918550672141d324a3813d8283d48  fix-embedded_interpreter.patch
14c556d495696abb303744f414fa4145fb00e7c3a8b42f09e4e4a5c84e02ff82efca31fd4cfda4a62b3a6e33efdb98042757eb5c46688acfcdfa81c0f321883d  musl-compat.patch
"
