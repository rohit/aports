# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=gst-plugins-good
pkgver=1.20.2
pkgrel=2
pkgdesc="GStreamer streaming media framework good plug-ins"
url="https://gstreamer.freedesktop.org"
arch="all"
license="GPL-2.0-or-later LGPL-2.0-or-later"
subpackages="$pkgname-lang $pkgname-gtk $pkgname-qt"
replaces="gst-plugins-good1"
makedepends="cairo-dev flac-dev gdk-pixbuf-dev glib-dev gst-plugins-base-dev
	gstreamer-dev gtk+3.0-dev jack-dev lame-dev libavc1394-dev libcaca-dev
	libdv-dev libgudev-dev libice-dev libiec61883-dev libjpeg-turbo-dev
	libogg-dev libpng-dev libshout-dev libsm-dev libsoup-dev libvpx-dev
	libxdamage-dev libxext-dev libxv-dev linux-headers meson mpg123-dev
	orc-compiler orc-dev taglib-dev v4l-utils-dev wavpack-dev zlib-dev
	pulseaudio-dev qt5-qtdeclarative-dev qt5-qtx11extras-dev"
ldpath="/usr/lib/gstreamer-1.0"
source="https://gstreamer.freedesktop.org/src/gst-plugins-good/gst-plugins-good-$pkgver.tar.xz"

case "$CARCH" in
armhf|s390x|ppc64le)
	# really flaky tests
	options="$options !check"
	;;
*)
	;;
esac

# secfixes:
#   1.18.4-r0:
#     - CVE-2021-3497
#     - CVE-2021-3498
#   1.10.4-r0:
#     - CVE-2017-5840
#     - CVE-2017-5841
#     - CVE-2017-5845
#     - CVE-2016-9634
#     - CVE-2016-9635
#     - CVE-2016-9636
#     - CVE-2016-9808
#     - CVE-2016-10198
#     - CVE-2016-10199

build() {
	abuild-meson \
		-Dpackage-origin="https://alpinelinux.org" \
		-Dpackage-name="GStreamer good plug-ins (Alpine Linux)" \
		-Dtests="$(want_check && echo enabled || echo disabled)" \
		. output
	meson compile ${JOBS:+-j ${JOBS}} -C output
}

check() {
	meson test -t 2 -v --no-rebuild -C output
}

package() {
	export GCONF_DISABLE_MAKEFILE_SCHEMA_INSTALL=1
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

_mv() {
	mkdir -p "$subpkgdir"/"$ldpath"
	mv "$1" "$subpkgdir"/"$ldpath"
}

qt() {
	pkgdesc="Gstreamer Qt plug-in"
	find "$pkgdir" -type f | while read -r i; do
		if ldd $i 2>/dev/null | grep -q "libQt"; then
			_mv "$i"
		fi
	done
}

gtk() {
	pkgdesc="Gstreamer gtk-3 plug-in"
	find "$pkgdir" -type f | while read -r i; do
		if ldd $i 2>/dev/null | grep -q "libgtk-3"; then
			_mv "$i"
		fi
	done
}

sha512sums="
a10ea48fdfbe741e9bc63036b011748558f89968aaa525380ac99884c27463d190c0b49e2d98b4f554eee6ead8a5c5da3ba62e5b0b1fe54877598beccc68a933  gst-plugins-good-1.20.2.tar.xz
"
