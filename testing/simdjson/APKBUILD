# Contributor: Grigory Kirillov <txgk@bk.ru>
# Maintainer: Grigory Kirillov <txgk@bk.ru>
pkgname=simdjson
pkgver=2.0.3
pkgrel=0
pkgdesc="Parsing gigabytes of JSON per second"
url="https://simdjson.org"
arch="all"
license="Apache-2.0"
# tests take a very long time to compile and require downloading other JSON parsers
options="!check"
makedepends="cmake samurai"
subpackages="$pkgname-static $pkgname-dev"
source="https://github.com/simdjson/simdjson/archive/v$pkgver/simdjson-$pkgver.tar.gz"

build() {
	cmake -B builddir -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_BUILD_TYPE=Release \
		-DBUILD_SHARED_LIBS=ON
	cmake -B builddir-static -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_BUILD_TYPE=Release
	cmake --build builddir
	cmake --build builddir-static
}

package() {
	DESTDIR="$pkgdir" cmake --install builddir
	DESTDIR="$pkgdir" cmake --install builddir-static
}

sha512sums="
f07de33af3d1489d2597e531ef3edc416dd1491a72b5e863d906e3a3a37a0dc5a2656de31e59e6f0597237bc765f3c85626cd0b6a7ef704c2fd25866f2ad79be  simdjson-2.0.3.tar.gz
"
