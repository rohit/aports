# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=difftastic
pkgver=0.29.0
pkgrel=0
pkgdesc="Diff tool that understands syntax"
url="https://difftastic.wilfred.me.uk/"
license="MIT"
arch="all !s390x !riscv64" # blocked by rust/cargo
arch="$arch !aarch64 !armhf !armv7" # FTBFS on arm
makedepends="cargo openssl-dev"
source="https://github.com/Wilfred/difftastic/archive/$pkgver/difftastic-$pkgver.tar.gz"

prepare() {
	default_prepare
	cargo fetch --locked
}

build() {
	cargo build --frozen --release
}

check() {
	cargo test --frozen
}

package() {
	install -Dm755 target/release/difft -t "$pkgdir"/usr/bin
}

sha512sums="
dd3e58f9ab8682fad2c167729808a2b7616fb6b7049b9fff28bf11f00bd6d1f8c55aadd332d906f5e4a66b9c2dbc05420f490a4716194688d71398a630bd42bf  difftastic-0.29.0.tar.gz
"
